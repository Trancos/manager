var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var Client = require('ssh2').Client;

var ip = '172.17.0.1';
var port = 22;
var username = 'manuel';
var key = require('fs').readFileSync('/root/.ssh/id_rsa');


app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
    socket.on('log message', function(data) {
        if (data.task === 'project') {
            buildProject(data);
        }
    });
});

var buildProject = function(build) {
    var conn = new Client();
    conn.on('ready', function() {
        console.log('SSH is ready');

        var command = 'cd workspace && ./build.sh ' + build.service;

        build.parameters.forEach(function(value){
            if(value != ''){
                command = command + ' ' + value;
            }
        });

        console.log("COMMAND: " + command);
        io.emit('log message', "[" + (new Date).toLocaleTimeString() + "] EXEC: " + command);

        conn.exec(command, function(err, stream) {
            if (err) throw err;
            stream.on('close', function(code, signal) {
                console.log('SSH is close : code: ' + code + ', signal: ' + signal);
                conn.end();
            }).on('data', function(data) {
                // console.log('STDOUT: ' + data);
                io.emit('log message', "[" + (new Date).toLocaleTimeString() + "] " + data);
            }).stderr.on('data', function(data) {
                console.log('STDERR: ' + data);
            });
        });
    }).connect({
        host: ip,
        port: port,
        username: username,
        privateKey: key
    });
}

http.listen(3000, function() {
    console.log('listening on *:3000');
});